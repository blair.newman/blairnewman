# Blair Newman

## Morehouse College Computer Science graduate 

Experienced in working with electronic health records reporting, software and webpage development. I am searching for a role where I can use and sharpen my skills through the fusion of academic knowledge with practical experience and constructively contribute to the organization by bringing innovative technology solutions to life.

## Hobbies and Interests
Basketball, playing video games, watching anime, reading manga

## Experiences
-Student Intern 
UCSF Health
-Server
Slow Hand BBQ 
-Server
Devino's Pizza and Pasta · 
